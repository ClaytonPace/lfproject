﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed;
    private Transform playerPosition;
    private Player player;
    public GameObject effect;

    /*
    public AudioClip damageEnemy;//Check Audio
    AudioSource audioSource;
    */

    void Start()
    {
        //audioSource = GetComponent<AudioSource>();//Check Audio

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, playerPosition.position, speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D bullet)
    {
        if (bullet.CompareTag("Player"))
        {
            //Particle effect
            Instantiate(effect, transform.position, Quaternion.identity);
            //player lose health
            player.health--;

            Debug.Log(player.health);

            //Destroy enemy on collision
           //audioSource.PlayOneShot(damageEnemy);//Check Audio
            ScoreScript.scoreValue -= 5;
            Destroy(gameObject);
 
        }

        if (bullet.CompareTag("Bullets"))
        {
            //Particle effect
            ScoreScript.scoreValue += 5;
            Instantiate(effect, transform.position, Quaternion.identity);
            Destroy(bullet.gameObject);
            Destroy(gameObject);
        }
    }
}

 
