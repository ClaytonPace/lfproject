﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public float speed;
    public float stopDistanceFromPlay;
    public float retreatDistanceFromPlay;

    private float timeBetweenShooting;
    public float startTimeBetweenShooting;

    public GameObject projetile;
    private Transform player;

    public GameObject effect;

    //public AudioSource damageEnemy;//Check Audio

    // Use this for initialization
    void Start ()
    {
        //damageEnemy = GetComponent<AudioSource>();//Check Audio

        player = GameObject.FindGameObjectWithTag("Player").transform;

        timeBetweenShooting = startTimeBetweenShooting;
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector2.Distance(transform.position, player.position) > stopDistanceFromPlay)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }

        else if (Vector2.Distance(transform.position, player.position) < stopDistanceFromPlay && Vector2.Distance(transform.position, player.position) > retreatDistanceFromPlay)
        {
            transform.position = this.transform.position;
        }

		else if(Vector2.Distance(transform.position, player.position) < retreatDistanceFromPlay)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }

        if (timeBetweenShooting <= 0)
        {
            Instantiate(projetile, transform.position, Quaternion.identity);
            timeBetweenShooting = startTimeBetweenShooting;

        }
        else
        {
            timeBetweenShooting -= Time.deltaTime;
        }
	}

    void OnTriggerEnter2D(Collider2D bullet)
    {
      
        if (bullet.CompareTag("Bullets"))
        {
            //Particle effect
            //damageEnemy.Play();//Check Audio
            ScoreScript.scoreValue += 10;
            Instantiate(effect, transform.position, Quaternion.identity);
            Destroy(bullet.gameObject);
            Destroy(gameObject);
        }
    }
}
