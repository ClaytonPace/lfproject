﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIBullet : MonoBehaviour {

    public float speed;

    private Transform playerPos;
    private Vector2 target;
    private Player player;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        playerPos = GameObject.FindGameObjectWithTag("Player").transform;

        target = new Vector2(playerPos.position.x, playerPos.position.y);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (transform.position.x == target.x && transform.position.y == target.y)
        {
            DestroyEnemyBullet();
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //destroy enemy bullet on collisior with player
            DestroyEnemyBullet();

            //Player health reduced whed hit by enemy bullet
            player.health--;
            ScoreScript.scoreValue -= 5;
        }
    }

    void DestroyEnemyBullet()
    {
        
        Destroy(gameObject);
    }

}
