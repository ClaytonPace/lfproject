﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;

    public Transform[] spawnPoints;

    private float timeBetweenSpawn;

    public float starTimeSpawns;

    void Start()
    {
        timeBetweenSpawn = starTimeSpawns;
    }

    void Update()
    {
        if (timeBetweenSpawn <= 0)
        {
            int randomPositions = Random.Range(0, spawnPoints.Length -1);
            Instantiate(enemy, spawnPoints[randomPositions].position, Quaternion.identity);
            timeBetweenSpawn = starTimeSpawns;
        }
        else
        {
            timeBetweenSpawn -= Time.deltaTime;
        }
    }
}
