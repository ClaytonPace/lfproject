﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    public GameObject fire;
    private Transform playersPosition;

    void Start()
    {
        playersPosition = GetComponent<Transform>();
    }

	// Update is called once per frame
	void Update ()
    {
        

        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(fire, playersPosition.position, Quaternion.identity);
        }
	}
   
}
