﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerTest : MonoBehaviour {

    public float speed;
    public int health = 10;
    private Rigidbody2D rb;
    private Vector2 moveVelocity;
    public Text playerHealth;

    //Rotation
    public float rotationSpeed;

    //For Bounderies
    public float padding = 0.5f;
    float xMin, xMax;
    float yMin, yMax;

    //-https://www.youtube.com/watch?v=lkDGk3TjsIE

    void Start()
    {
        SetupMoveBoundaries();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        faceMouse();
        Move();

        playerHealth.text = "Health : " + health;

        if (health <= 0)
        {
            SceneManager.LoadScene("Lose"); //Health = or less than zero show lose scene
        }
    }

    //Rotation With mouse position
    void faceMouse()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    private void Move()
    {
        //var is a generic variable which changes its type
        //depending on the variable
        //deltaX: saves the differance travelled in x-axis
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        //newXPos: saves the new position
        //Clamps the position of the spaceship between xMin and xMax
        var newXPos = Mathf.Clamp(this.transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(this.transform.position.y + deltaY, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);
    }

    void SetupMoveBoundaries()
    {
        //gameCamera = main Unity camera
        Camera gameCamera = Camera.main;

        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;

        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }
}
