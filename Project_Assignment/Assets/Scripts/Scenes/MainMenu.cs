﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void StartGame() //To start the first level
    {
        SceneManager.LoadScene("Level1");
    }

    public void QuitGame() //Unity will quit after quit is clicked
    {
        Debug.Log("Quit pressed");//Remove
        Application.Quit();
    }
}
